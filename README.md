# [recipes2](https://gitlab.com/eidoom/recipes2)

Initialised with [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte).

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

## Deploying

You can deploy the production build with

```shell
HOST=127.0.0.1 \
PORT=3000 \
PROTOCOL_HEADER=x-forwarded-proto \
HOST_HEADER=x-forwarded-host \
node build
```

Here's a sample `nginx` proxy server (using [`__tls_wildcard_duckdns`](https://gitlab.com/eidoom/cloud-infrastructure/-/blob/main/type/__tls_wildcard_duckdns/gencode-remote)): `/etc/nginx/sites-available/recipes.conf`
```nginx
server {
    listen 80;
    listen [::]:80;

    server_name recipes.DOMAIN;

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name recipes.DOMAIN;

    ssl_certificate /etc/letsencrypt/live/star.DOMAIN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/star.DOMAIN/privkey.pem;

    location / {
        proxy_pass http://127.0.0.1:3000/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

## Debian Bullseye (11) [stable] dependencies

Install:
```vim
sqlite3
```

Unfortunately, have to install `nodejs` from `bookworm` (12) [unstable] to get a sufficiently up-to-date version for `sveltekit`:
```vim
nodejs
node-https-proxy-agent
node-gauge
node-has-unicode
node-set-blocking
node-validate-npm-package-name
node-imurmurhash
node-promise-inflight
node-fs.realpath
node-inflight
node-wrappy
node-once
node-util-deprecate
npm
```

## TODOs

* How to handle singular/plural names of items?
* Hide `manage` behind `admin` account or similar
* Items: record add/edit datetimes?
