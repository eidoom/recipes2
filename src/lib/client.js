export function check_title_unique({ target }, names) {
  if (names.includes(target.value)) {
    target.setCustomValidity("This name already exists!");
  } else {
    target.setCustomValidity("");
  }
}
