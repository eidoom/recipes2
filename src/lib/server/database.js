import { fail } from "@sveltejs/kit";
import Database from "better-sqlite3";

export const db = new Database("recipes.db");
export default db;

export function prev_next(table, id) {
  return {
    prev: db
      .prepare(
        `
        SELECT id
        FROM ${table}
        WHERE id < ?
        ORDER BY id DESC
        LIMIT 1
        `
      )
      .pluck()
      .get(id),
    next: db
      .prepare(
        `
        SELECT id
        FROM ${table}
        WHERE id > ?
        ORDER BY id ASC
        LIMIT 1
        `
      )
      .pluck()
      .get(id),
  };
}

export function get_ingredient_sets(id) {
  const ingredient_sets = db
    .prepare(
      `
      SELECT
        id,
        name
      FROM ingredient_set
      WHERE recipe_id = ?
      ORDER BY name ASC
      `
    )
    .all(id);

  for (let ingredient_set of ingredient_sets) {
    ingredient_set.ingredients = db
      .prepare(
        `
      SELECT
        i.id,
        q.quantity,
        w.name AS unit,
        i.name,
        c.comment,
        IFNULL(o.id, 0) AS optional
      FROM ingredient AS i
      INNER JOIN set_ingredient AS j ON j.ingredient_id = i.id
      LEFT OUTER JOIN ingredient_quantity AS q ON q.set_ingredient_id = j.id
      LEFT OUTER JOIN ingredient_unit AS u ON u.set_ingredient_id = j.id
      LEFT OUTER JOIN unit AS w ON u.unit_id = w.id
      LEFT OUTER JOIN ingredient_comment AS c ON c.set_ingredient_id = j.id
      LEFT OUTER JOIN ingredient_optional AS o ON o.set_ingredient_id = j.id
      WHERE j.ingredient_set_id = ?
      ORDER BY i.name ASC
      `
      )
      .all(ingredient_set.id);
  }

  return ingredient_sets;
}

export function delete_item(table, id) {
  try {
    db.prepare(`DELETE FROM ${table} WHERE id = ?`).run(id);
  } catch (err) {
    return fail(422, { error: { name: err.name, message: err.message } });
  }
}
