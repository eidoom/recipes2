import db from "$lib/server/database";

db.pragma("foreign_keys = ON");

db.prepare(`
CREATE TABLE IF NOT EXISTS unit (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT NOT NULL UNIQUE
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS recipe (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT NOT NULL UNIQUE,
  desc TEXT NOT NULL,
  makes_quantity REAL NOT NULL,
  makes_unit_id INTEGER NOT NULL,
  added INTEGER NOT NULL DEFAULT (UNIXEPOCH()),
  edited INTEGER NOT NULL DEFAULT (UNIXEPOCH()),
  FOREIGN KEY (makes_unit_id) REFERENCES unit (id) ON DELETE RESTRICT
) STRICT
`).run();

// db.prepare(`
// CREATE TABLE IF NOT EXISTS link (
//   id INTEGER PRIMARY KEY NOT NULL,
//   recipe_id INTEGER NOT NULL,
//   name TEXT NOT NULL,
//   link TEXT NOT NULL,
//   FOREIGN KEY (recipe_id) REFERENCES recipe (id) ON DELETE CASCADE
// ) STRICT
// `).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS method (
  id INTEGER PRIMARY KEY NOT NULL,
  step INTEGER NOT NULL,
  desc TEXT NOT NULL,
  recipe_id INTEGER NOT NULL,
  FOREIGN KEY (recipe_id) REFERENCES recipe (id) ON DELETE CASCADE
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS ingredient (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT NOT NULL UNIQUE,
  desc TEXT NOT NULL DEFAULT ''
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS ingredient_set (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT NOT NULL DEFAULT '',
  recipe_id INTEGER NOT NULL,
  FOREIGN KEY (recipe_id) REFERENCES recipe (id) ON DELETE CASCADE
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS set_ingredient (
  id INTEGER PRIMARY KEY NOT NULL,
  ingredient_id INTEGER NOT NULL,
  ingredient_set_id INTEGER NOT NULL,
  FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) ON DELETE RESTRICT,
  FOREIGN KEY (ingredient_set_id) REFERENCES ingredient_set (id) ON DELETE CASCADE
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS ingredient_quantity (
  id INTEGER PRIMARY KEY NOT NULL,
  set_ingredient_id INTEGER NOT NULL,
	quantity REAL NOT NULL,
  FOREIGN KEY (set_ingredient_id) REFERENCES set_ingredient (id) ON DELETE CASCADE
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS ingredient_unit (
  id INTEGER PRIMARY KEY NOT NULL,
  set_ingredient_id INTEGER NOT NULL,
	unit_id INTEGER NOT NULL,
  FOREIGN KEY (set_ingredient_id) REFERENCES set_ingredient (id) ON DELETE CASCADE
  FOREIGN KEY (unit_id) REFERENCES unit (id) ON DELETE RESTRICT
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS ingredient_comment (
  id INTEGER PRIMARY KEY NOT NULL,
  set_ingredient_id INTEGER NOT NULL,
  comment TEXT NOT NULL,
  FOREIGN KEY (set_ingredient_id) REFERENCES set_ingredient (id) ON DELETE CASCADE
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS ingredient_optional (
  id INTEGER PRIMARY KEY NOT NULL,
  set_ingredient_id INTEGER NOT NULL,
  FOREIGN KEY (set_ingredient_id) REFERENCES set_ingredient (id) ON DELETE CASCADE
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS tag (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT NOT NULL UNIQUE,
  desc TEXT NOT NULL DEFAULT ''
) STRICT
`).run();

db.prepare(`
CREATE TABLE IF NOT EXISTS recipe_tag (
  id INTEGER PRIMARY KEY NOT NULL,
  tag_id INTEGER NOT NULL,
  recipe_id INTEGER NOT NULL,
  FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE RESTRICT,
  FOREIGN KEY (recipe_id) REFERENCES recipe (id) ON DELETE CASCADE
) STRICT
`).run();
