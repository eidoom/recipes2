import { error } from "@sveltejs/kit";
// import { fail } from "@sveltejs/kit";

import db from "$lib/server/database";

const allowed_order = ["name", "added", "edited"];

export function load({ params, locals }) {
  const order = locals.order ?? "added";

  const recipes = db
    .prepare(
      `
      SELECT
        id,
        name
      FROM recipe
      ORDER BY ${order}
      `
    )
    .all();

  return { recipes, allowed_order, order };
}

export const actions = {
  default: async ({ request, locals }) => {
    const data = await request.formData();
    const order = data.get("order");
    if (!allowed_order.includes(order)) {
      throw error(
        422,
        `You gave '${order}' for order, which must be one of ${allowed_order}`
      );
      // return fail(422);
    }
    locals.order = order;
  },
};
