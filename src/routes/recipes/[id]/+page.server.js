import { error } from "@sveltejs/kit";
import { db, prev_next, get_ingredient_sets } from "$lib/server/database";

export function load({ params }) {
  const recipe = db
    .prepare(
      `
      SELECT
        r.id,
        r.name,
        r.desc,
        r.makes_quantity,
        u.name AS makes_unit,
        r.added,
        r.edited
      FROM recipe AS r
      INNER JOIN unit AS u ON u.id = r.makes_unit_id
      WHERE r.id = @id
      `
    )
    .get(params);

  if (recipe === undefined) {
    throw error(404, "Not Found");
  }

  for (const date of ["added", "edited"]) {
    recipe[date] = new Date(1000 * recipe[date]);
  }

  const tags = db
    .prepare(
      `
      SELECT
        t.id,
        t.name
      FROM tag AS t
      INNER JOIN recipe_tag AS r ON r.tag_id = t.id
      WHERE r.recipe_id = @id
      ORDER BY t.name ASC
      `
    )
    .all(params);

  const method = db
    .prepare(
      `
      SELECT
        desc
      FROM method
      WHERE recipe_id = @id
      ORDER BY step ASC
      `
    )
    .pluck()
    .all(params);

  const ingredient_sets = get_ingredient_sets(params.id);

  const nav = prev_next("recipe", params.id);

  return { recipe, method, tags, ingredient_sets, nav };
}
