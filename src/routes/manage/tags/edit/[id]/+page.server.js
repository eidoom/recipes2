import { fail, redirect } from "@sveltejs/kit";
import db from "$lib/server/database";

export async function load({ params }) {
  const names = db
    .prepare("SELECT name FROM tag WHERE NOT id = @id")
    .pluck()
    .all(params);

  const item = db
    .prepare(
      `
      SELECT
        name,
        desc
      FROM tag
      WHERE id = @id
      `
    )
    .get(params);

  if (item === undefined) {
    throw error(404, "Not Found");
  }

  return { names, item };
}

export const actions = {
  default: async ({ request, params }) => {
    const data = await request.formData();

    const name = data.get("name");
    if (!name) {
      throw new Error("Missing title");
    }
    const desc = data.get("desc");

    try {
      db.prepare(
        `
          UPDATE tag
          SET
            name = ?,
            desc = ?
          WHERE
            id = ?
          `
      ).run(name, desc, params.id);
    } catch (err) {
      return fail(422, { error: { name: err.name, message: err.message } });
    }

    throw redirect(303, `/tags/${params.id}`);
  },
};
