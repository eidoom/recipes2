import { fail, redirect } from "@sveltejs/kit";
import db from "$lib/server/database";

export async function load() {
  const all_tags = db.prepare("SELECT name FROM tag").pluck().all();
  const ingredients = db.prepare("SELECT name FROM ingredient").pluck().all();
  const units = db.prepare("SELECT name FROM unit").pluck().all();
  const names = db.prepare("SELECT name FROM recipe").pluck().all();

  const method = [],
    tags = [],
    ingredient_sets = [],
    recipe = { name: "", desc: "", makes_quantity: "", makes_unit: "" };

  return {
    all_tags,
    ingredients,
    units,
    names,
    method,
    tags,
    ingredient_sets,
    recipe,
  };
}

export const actions = {
  default: async ({ request }) => {
    const transaction = db.transaction((data) => {
      // recipe

      const name = data.get("name");
      if (!name) {
        throw new Error("Missing title");
      }
      const desc = data.get("desc");
      const makes_quantity = data.get("makes_quantity");
      if (!makes_quantity) {
        throw new Error("Missing makes quantity");
      }
      const makes_unit_name = data.get("makes_unit");
      if (!makes_unit_name) {
        throw new Error("Missing makes unit");
      }

      let makes_unit_id = db
        .prepare("SELECT id FROM unit WHERE name = ?")
        .pluck()
        .get(makes_unit_name);

      if (makes_unit_id === undefined) {
        makes_unit_id = db
          .prepare("INSERT INTO unit (name) VALUES (?)")
          .run(makes_unit_name).lastInsertRowid;
      }

      const recipe_id = db
        .prepare(
          `
        INSERT INTO recipe (name, desc, makes_quantity, makes_unit_id)
          VALUES (?, ?, ?, ?)
        `
        )
        .run(name, desc, makes_quantity, makes_unit_id).lastInsertRowid;

      // ingredient

      let j = 0;
      while (data.has(`ingredient_name_${j}_0`)) {
        let i = 0;

        let ingredient_set_id;
        if (data.has(`ingredient_set_name_${j}`)) {
          const ingredient_set_name = data.get(`ingredient_set_name_${j}`);
          ingredient_set_id = db
            .prepare(
              "INSERT INTO ingredient_set (name, recipe_id) VALUES (?, ?)"
            )
            .run(ingredient_set_name, recipe_id).lastInsertRowid;
        } else {
          ingredient_set_id = db
            .prepare("INSERT INTO ingredient_set (recipe_id) VALUES (?)")
            .run(recipe_id).lastInsertRowid;
        }

        while (data.has(`ingredient_name_${j}_${i}`)) {
          const ingredient_name = data.get(`ingredient_name_${j}_${i}`);

          let ingredient_id = db
            .prepare("SELECT id FROM ingredient WHERE name = ?")
            .pluck()
            .get(ingredient_name);

          if (ingredient_id === undefined) {
            ingredient_id = db
              .prepare("INSERT INTO ingredient (name) VALUES (?)")
              .run(ingredient_name).lastInsertRowid;
          }

          const set_ingredient_id = db
            .prepare(
              "INSERT INTO set_ingredient (ingredient_id, ingredient_set_id) VALUES (?, ?)"
            )
            .run(ingredient_id, ingredient_set_id).lastInsertRowid;

          const ingredient_quantity = data.get(`ingredient_quantity_${j}_${i}`);
          if (ingredient_quantity) {
            db.prepare(
              "INSERT INTO ingredient_quantity (quantity, set_ingredient_id) VALUES (?, ?)"
            ).run(ingredient_quantity, set_ingredient_id);
          }

          const ingredient_unit = data.get(`ingredient_unit_${j}_${i}`);
          if (ingredient_unit) {
            let ingredient_unit_id = db
              .prepare("SELECT id FROM unit WHERE name = ?")
              .pluck()
              .get(ingredient_unit);

            if (ingredient_unit_id === undefined) {
              ingredient_unit_id = db
                .prepare("INSERT INTO unit (name) VALUES (?)")
                .run(ingredient_unit).lastInsertRowid;
            }

            db.prepare(
              "INSERT INTO ingredient_unit (unit_id, set_ingredient_id) VALUES (?, ?)"
            ).run(ingredient_unit_id, set_ingredient_id);
          }

          const ingredient_comment = data.get(`ingredient_comment_${j}_${i}`);
          if (ingredient_comment) {
            db.prepare(
              "INSERT INTO ingredient_comment (comment, set_ingredient_id) VALUES (?, ?)"
            ).run(ingredient_comment, set_ingredient_id);
          }

          if (data.has(`ingredient_optional_${j}_${i}`)) {
            db.prepare(
              "INSERT INTO ingredient_optional (set_ingredient_id) VALUES (?)"
            ).run(set_ingredient_id);
          }

          i += 1;
        }

        j += 1;
      }

      // method

      for (const [i, method] of data.getAll("method").entries()) {
        db.prepare(
          "INSERT INTO method (step, desc, recipe_id) VALUES (?, ?, ?)"
        ).run(i, method, recipe_id);
      }

      // tag

      for (const tag_name of data.getAll("tag")) {
        let tag_id = db
          .prepare("SELECT id FROM tag WHERE name = ?")
          .pluck()
          .get(tag_name);

        if (tag_id === undefined) {
          tag_id = db
            .prepare("INSERT INTO tag (name) VALUES (?)")
            .run(tag_name).lastInsertRowid;
        }

        db.prepare(
          "INSERT INTO recipe_tag (tag_id, recipe_id) VALUES (?, ?)"
        ).run(tag_id, recipe_id);
      }

      return recipe_id;
    });

    const request_data = await request.formData();

    let id;

    try {
      id = transaction(request_data);
    } catch (err) {
      return fail(422, { error: { name: err.name, message: err.message } });
    }

    throw redirect(303, `/recipes/${id}`);
  },
};
