import { fail, redirect } from "@sveltejs/kit";
import { db, get_ingredient_sets } from "$lib/server/database";

export async function load({ params }) {
  const all_tags = db.prepare("SELECT name FROM tag").pluck().all();
  const ingredients = db.prepare("SELECT name FROM ingredient").pluck().all();
  const units = db.prepare("SELECT name FROM unit").pluck().all();
  const names = db
    .prepare("SELECT name FROM recipe WHERE NOT id = @id")
    .pluck()
    .all(params);

  const recipe = db
    .prepare(
      `
      SELECT
        r.id,
        r.name,
        r.desc,
        r.makes_quantity,
        u.name AS makes_unit
      FROM recipe AS r
      INNER JOIN unit AS u ON u.id = r.makes_unit_id
      WHERE r.id = @id
      `
    )
    .get(params);

  if (recipe === undefined) {
    throw error(404, "Not Found");
  }

  const tags =
    db
      .prepare(
        `
      SELECT
        name
      FROM tag AS t
      INNER JOIN recipe_tag AS r ON r.tag_id = t.id
      WHERE r.recipe_id = @id
      ORDER BY t.name ASC
      `
      )
      .pluck()
      .all(params) || [];

  const method =
    db
      .prepare(
        `
      SELECT
        desc
      FROM method
      WHERE recipe_id = @id
      ORDER BY step ASC
      `
      )
      .pluck()
      .all(params) || [];

  const ingredient_sets = get_ingredient_sets(params.id);

  return {
    all_tags,
    ingredients,
    units,
    names,
    recipe,
    tags,
    method,
    ingredient_sets,
  };
}

export const actions = {
  default: async ({ request, params }) => {
    const transaction = db.transaction((data) => {
      // recipe

      const edited = Math.floor(Date.now() / 1000);
      const name = data.get("name");
      if (!name) {
        throw new Error("Missing title");
      }
      const desc = data.get("desc");
      const makes_quantity = data.get("makes_quantity");
      if (!makes_quantity) {
        throw new Error("Missing makes quantity");
      }
      const makes_unit_name = data.get("makes_unit");
      if (!makes_unit_name) {
        throw new Error("Missing makes unit");
      }

      let makes_unit_id = db
        .prepare("SELECT id FROM unit WHERE name = ?")
        .pluck()
        .get(makes_unit_name);

      if (makes_unit_id === undefined) {
        makes_unit_id = db
          .prepare("INSERT INTO unit (name) VALUES (?)")
          .run(makes_unit_name).lastInsertRowid;
      }

      db.prepare(
        `
          UPDATE recipe
          SET
            name = ?,
            desc = ?,
            makes_quantity = ?,
            makes_unit_id = ?,
            edited = ?
          WHERE
            id = ?
          `
      ).run(name, desc, makes_quantity, makes_unit_id, edited, params.id);

      // ingredient

      db.prepare("DELETE FROM ingredient_set WHERE recipe_id = @id").run(
        params
      );

      let j = 0;
      while (data.has(`ingredient_name_${j}_0`)) {
        let i = 0;

        let ingredient_set_id;
        if (data.has(`ingredient_set_name_${j}`)) {
          const ingredient_set_name = data.get(`ingredient_set_name_${j}`);
          ingredient_set_id = db
            .prepare(
              "INSERT INTO ingredient_set (name, recipe_id) VALUES (?, ?)"
            )
            .run(ingredient_set_name, params.id).lastInsertRowid;
        } else {
          ingredient_set_id = db
            .prepare("INSERT INTO ingredient_set (recipe_id) VALUES (?)")
            .run(params.id).lastInsertRowid;
        }

        while (data.has(`ingredient_name_${j}_${i}`)) {
          const ingredient_name = data.get(`ingredient_name_${j}_${i}`);

          let ingredient_id = db
            .prepare("SELECT id FROM ingredient WHERE name = ?")
            .pluck()
            .get(ingredient_name);

          if (ingredient_id === undefined) {
            ingredient_id = db
              .prepare("INSERT INTO ingredient (name) VALUES (?)")
              .run(ingredient_name).lastInsertRowid;
          }

          const set_ingredient_id = db
            .prepare(
              "INSERT INTO set_ingredient (ingredient_id, ingredient_set_id) VALUES (?, ?)"
            )
            .run(ingredient_id, ingredient_set_id).lastInsertRowid;

          const ingredient_quantity = data.get(`ingredient_quantity_${j}_${i}`);
          if (ingredient_quantity) {
            db.prepare(
              "INSERT INTO ingredient_quantity (quantity, set_ingredient_id) VALUES (?, ?)"
            ).run(ingredient_quantity, set_ingredient_id);
          }

          const ingredient_unit = data.get(`ingredient_unit_${j}_${i}`);
          if (ingredient_unit) {
            let ingredient_unit_id = db
              .prepare("SELECT id FROM unit WHERE name = ?")
              .pluck()
              .get(ingredient_unit);

            if (ingredient_unit_id === undefined) {
              ingredient_unit_id = db
                .prepare("INSERT INTO unit (name) VALUES (?)")
                .run(ingredient_unit).lastInsertRowid;
            }

            db.prepare(
              "INSERT INTO ingredient_unit (unit_id, set_ingredient_id) VALUES (?, ?)"
            ).run(ingredient_unit_id, set_ingredient_id);
          }

          const ingredient_comment = data.get(`ingredient_comment_${j}_${i}`);
          if (ingredient_comment) {
            db.prepare(
              "INSERT INTO ingredient_comment (comment, set_ingredient_id) VALUES (?, ?)"
            ).run(ingredient_comment, set_ingredient_id);
          }

          if (data.has(`ingredient_optional_${j}_${i}`)) {
            db.prepare(
              "INSERT INTO ingredient_optional (set_ingredient_id) VALUES (?)"
            ).run(set_ingredient_id);
          }

          i += 1;
        }

        j += 1;
      }

      // method

      db.prepare("DELETE FROM method WHERE recipe_id = @id").run(params);

      for (const [i, method] of data.getAll("method").entries()) {
        db.prepare(
          "INSERT INTO method (step, desc, recipe_id) VALUES (?, ?, ?)"
        ).run(i, method, params.id);
      }

      // tag

      // TODO is there a better way to update this?
      db.prepare("DELETE FROM recipe_tag WHERE recipe_id = @id").run(params);

      for (const tag_name of data.getAll("tag")) {
        let tag_id = db
          .prepare("SELECT id FROM tag WHERE name = ?")
          .pluck()
          .get(tag_name);

        if (tag_id === undefined) {
          tag_id = db
            .prepare("INSERT INTO tag (name) VALUES (?)")
            .run(tag_name).lastInsertRowid;
        }

        db.prepare(
          "INSERT INTO recipe_tag (tag_id, recipe_id) VALUES (?, ?)"
        ).run(tag_id, params.id);
      }
    });

    const request_data = await request.formData();

    try {
      transaction(request_data);
    } catch (err) {
      return fail(422, { error: { name: err.name, message: err.message } });
    }

    throw redirect(303, `/recipes/${params.id}`);
  },
};
