import { delete_item } from "$lib/server/database";

export const actions = {
  default: async ({ params }) => {
    delete_item("recipe", params.id);
  },
};
