import db from "$lib/server/database";

export function load({ params }) {
  const recipes = db.prepare("SELECT id, name FROM recipe").all();
  const units = db
    .prepare(
      `
      SELECT
        i.id,
        i.name,
        COALESCE(j.id, k.id) AS used
      FROM unit AS i
      LEFT OUTER JOIN ingredient_unit AS j on j.set_ingredient_id = i.id
      LEFT OUTER JOIN recipe AS k on k.makes_unit_id = i.id
      GROUP BY i.id
      ORDER BY i.id
      `
    )
    .all();
  const tags = db
    .prepare(
      `
      SELECT
        i.id,
        i.name,
        j.id AS used
      FROM tag AS i
      LEFT OUTER JOIN recipe_tag AS j on j.tag_id = i.id
      GROUP BY i.id
      ORDER BY i.id
      `
    )
    .all();
  const ingredients = db
    .prepare(
      `
      SELECT
        i.id,
        i.name,
        j.id AS used
      FROM ingredient AS i
      LEFT OUTER JOIN set_ingredient AS j on j.ingredient_id = i.id
      GROUP BY i.id
      ORDER BY i.id
      `
    )
    .all();

  return { recipes, units, tags, ingredients };
}
