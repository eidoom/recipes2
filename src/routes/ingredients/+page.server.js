import db from "$lib/server/database";

export function load({ params }) {
  const ingredients = db.prepare("SELECT id, name FROM ingredient").all();

  return { ingredients };
}
