import { error } from "@sveltejs/kit";
import { db, prev_next } from "$lib/server/database";

export function load({ params }) {
  const ingredient = db
    .prepare(
      `
      SELECT
        name,
        desc
      FROM ingredient
      WHERE ingredient.id = @id
      `
    )
    .get(params);

  if (ingredient === undefined) {
    throw error(404, "Not Found");
  }

  const tags = db
    .prepare(
      `
      SELECT
        tag.id,
        tag.name
      FROM tag
      INNER JOIN recipe_tag ON recipe_tag.tag_id = tag.id
      INNER JOIN ingredient_set ON ingredient_set.recipe_id = recipe_tag.recipe_id
      INNER JOIN set_ingredient ON set_ingredient.ingredient_set_id = ingredient_set.id
      WHERE set_ingredient.ingredient_id = @id
      ORDER BY tag.name ASC
      `
    )
    .all(params);

  const recipes = db
    .prepare(
      `
      SELECT
        recipe.id,
        recipe.name
      FROM recipe
      INNER JOIN ingredient_set ON ingredient_set.recipe_id = recipe.id
      INNER JOIN set_ingredient ON set_ingredient.ingredient_set_id = ingredient_set.id
      WHERE set_ingredient.ingredient_id = @id
      ORDER BY recipe.name ASC
      `
    )
    .all(params);

  const nav = prev_next("ingredient", params.id);

  return { ingredient, recipes, tags, nav };
}
