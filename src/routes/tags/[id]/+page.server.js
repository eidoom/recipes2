import { error } from "@sveltejs/kit";
import { db, prev_next } from "$lib/server/database";

export function load({ params }) {
  const tag = db
    .prepare(
      `
      SELECT
        name,
        desc
      FROM tag
      WHERE tag.id = @id
      `
    )
    .get(params);

  if (tag === undefined) {
    throw error(404, "Not Found");
  }

  const recipes = db
    .prepare(
      `
      SELECT
        recipe.id,
        recipe.name
      FROM recipe
      INNER JOIN recipe_tag ON recipe_tag.recipe_id = recipe.id
      WHERE recipe_tag.tag_id = @id
      ORDER BY recipe.name ASC
      `
    )
    .all(params);

  const nav = prev_next("tag", params.id);

  return { tag, recipes, nav };
}
