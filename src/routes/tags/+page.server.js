import db from "$lib/server/database";

export function load({ params }) {
  const tags = db.prepare("SELECT id, name FROM tag").all();

  return { tags };
}
